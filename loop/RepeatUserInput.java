package loop;

import java.util.Scanner;

public class RepeatUserInput {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number1, number2;
        do {
            System.out.print("Enter the first number: ");
            number1 = scanner.nextInt();
            System.out.print("Enter the second number: ");
            number2 = scanner.nextInt();
            System.out.println("The sum is: " + (number1 + number2));
        } while (number1 != 0 || number2 != 0);
    }
}
