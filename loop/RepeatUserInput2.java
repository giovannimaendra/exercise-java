package loop;

import java.util.Scanner;

public class RepeatUserInput2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number1, number2;
        do {
            do {
                System.out.print("Enter the first number: ");
                number1 = scanner.nextInt();
                if (number1 < 0) {
                    System.out.println("Invalid input. Please enter a positive number or 0.");
                }
            } while (number1 < 0);
            do {
                System.out.print("Enter the second number: ");
                number2 = scanner.nextInt();
                if (number2 < 0) {
                    System.out.println("Invalid input. Please enter a positive number or 0.");
                }
            } while (number2 < 0);
            System.out.println("The sum is: " + (number1 + number2));
        } while (number1 != 0 || number2 != 0);
    }
}
