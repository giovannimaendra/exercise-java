package loop;

import java.util.Scanner;

public class InputPassword {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String password = "qwerty";
        while (true){
            System.out.print("Masukkan password : ");
            String inputPass = sc.next();
            if(password.equals(inputPass)){
                System.out.print("Password benar!!!");
                break;
            }
            System.out.println("Password salah");
        }
    }
}
