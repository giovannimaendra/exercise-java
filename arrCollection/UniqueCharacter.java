package arrCollection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class UniqueCharacter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input: ");
        String words = sc.nextLine();

        Set<Character> uniqueChars = new HashSet<>();
        for (char c : words.toCharArray()) {
            uniqueChars.add(c);
        }

        String unique = "";
        for (char v: words.toCharArray()){
            if(unique.indexOf(v) == -1){
                unique += v;
            }
        }

        System.out.println("Jumlah karakter unik: " + uniqueChars.size());
        System.out.println("Jumlah karakter unik: " + unique.length());
    }
}
