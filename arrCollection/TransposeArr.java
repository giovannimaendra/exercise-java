package arrCollection;

public class TransposeArr {
    public static void main(String[] args) {
        // Define the static matrix
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        // Create a new matrix for the transposed matrix
        int[][] transposedMatrix = new int[matrix[0].length][matrix.length];

        // Transpose the matrix
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                transposedMatrix[j][i] = matrix[i][j];
            }
        }

        // Print the transposed matrix
        System.out.println("Transposed Matrix:");
        for (int[] ints : transposedMatrix) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}
