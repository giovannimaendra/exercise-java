package arrCollection;

import java.util.Scanner;

public class ReverseString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input: ");
        String words = sc.nextLine();

        String[] splittedWord = words.split("");

        System.out.print("Output: ");
        for (int i = words.length()-1; i >= 0; i--) {
            System.out.print(splittedWord[i]);
        }
    }
}
