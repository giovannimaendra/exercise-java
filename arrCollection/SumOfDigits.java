package arrCollection;

import java.util.*;
public class SumOfDigits
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        int dig, res ;
        System.out.print("Enter the Digits : ");
        dig = input.nextInt();
        res = sum_Digits(dig);
        System.out.printf("Sum of Digits : " + res);
    }
    static int sum = 0;
    public static int sum_Digits(int dig)
    {
        if (dig > 0)
        {
            sum += (dig % 10);
            System.out.println("Sum of Digits : " + dig/10);
            sum_Digits(dig / 10);
        }
        return sum;
    }
}