package arrCollection;

public class JumlahArray2D {
    public static void main(String[] args) {
        // Membuat array 2 dimensi dengan nilai contoh
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        // Menghitung jumlah semua nilai dalam array 2D
        int total = 0;
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[i].length; j++) {
                total += array2D[i][j];
            }
        }

        // Mencetak hasil jumlahnya
        System.out.println("Jumlah semua nilai dalam array 2D adalah: " + total);
    }
}
