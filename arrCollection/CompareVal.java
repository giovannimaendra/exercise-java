package arrCollection;

import java.util.Scanner;

public class CompareVal {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan jumlah elemen dalam array: ");
        int n = input.nextInt();
        int[] arr = new int[n];

        System.out.println("Masukkan elemen array: ");
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }

        boolean found = false;
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if ((arr[i] + arr[j]) * 2 > sum) {
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }

        System.out.println(found ? "True" : "False");
    }
}
