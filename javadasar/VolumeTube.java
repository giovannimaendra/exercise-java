package javadasar;

import java.util.Scanner;

public class VolumeTube {
    public static void main(String[] args) {
        final double pi = 3.14;

        Scanner sc = new Scanner(System.in);
        System.out.print("Radius : ");
        int radius = sc.nextInt();
        System.out.print("Height : ");
        int height = sc.nextInt();
        sc.close();

        double area = pi*radius*radius*height;
        System.out.println(area);


    }
}
