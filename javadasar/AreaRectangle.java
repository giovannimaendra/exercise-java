package javadasar;

import java.util.Scanner;

public class AreaRectangle {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Length 1 : ");
        int length1 = sc.nextInt();
        System.out.print("Length 2 : ");
        int length2 = sc.nextInt();
        int area = length1*length2;
        System.out.println(area);
    }
}
