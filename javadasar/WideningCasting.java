package javadasar;

public class WideningCasting {
    public static void main(String[] args) {
        int num = 10;
        double data = num;
        System.out.println("The integer value: " + num);
        System.out.println("The double value: " + data);
    }
}
