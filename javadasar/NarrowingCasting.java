package javadasar;

public class NarrowingCasting {
    public static void main(String[] args) {
        double myDouble = 9.78;
        int myInt = (int) myDouble;
        System.out.println("The double value: " + myDouble);
        System.out.println("The integer value: " + myInt);
    }
}
