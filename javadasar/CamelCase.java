package javadasar;

import java.util.Scanner;

public class CamelCase {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama lengkap : ");
        String fullName = sc.nextLine();
        sc.close();

        String[] fullNameFormat = fullName.split(" ");
        String[] camelCase = new String[fullNameFormat.length];
        camelCase[0] = fullNameFormat[0].substring(0,1).toLowerCase() +
                fullNameFormat[0].substring(1).toLowerCase();
        for (int i = 1; i < fullNameFormat.length; i++){
            camelCase[i] = fullNameFormat[i].substring(0,1).toUpperCase() +
                    fullNameFormat[i].substring(1).toLowerCase();
        }

        System.out.println(String.join("",camelCase));
    }
}
