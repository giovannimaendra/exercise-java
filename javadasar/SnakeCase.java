package javadasar;

import java.util.Scanner;

public class SnakeCase {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama lengkap : ");
        String fullName = sc.nextLine();
        sc.close();

        String[] fullNameFormat = fullName.split(" ");
        String[] camelCase = new String[fullNameFormat.length];

        for (int i = 0; i < fullNameFormat.length; i++){
            camelCase[i] = fullNameFormat[i].toLowerCase();
        }

        System.out.println(String.join("_",camelCase));
    }
}
