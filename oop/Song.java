package oop;
//
//public class Lagu {
//
//    private String judul;
//    private String artis;
//    private int durasi;
//
//    public Lagu(String judul, String artis, int durasi) {
//        this.judul = judul;
//        this.artis = artis;
//        this.durasi = durasi;
//    }
//
//    public String getJudul() {
//        return judul;
//    }
//
//    public void setJudul(String judul) {
//        this.judul = judul;
//    }
//
//    public String getArtis() {
//        return artis;
//    }
//
//    public void setArtis(String artis) {
//        this.artis = artis;
//    }
//
//    public int getDurasi() {
//        return durasi;
//    }
//
//    public void setDurasi(int durasi) {
//        this.durasi = durasi;
//    }
//
//}
//
//public class DaftarPutar {
//
//    private String nama;
//    private String deskripsi;
//    private List<Lagu> lagu;
//    private int maxLagu;
//
//    public DaftarPutar(String nama, String deskripsi, int maxLagu) {
//        this.nama = nama;
//        this.deskripsi = deskripsi;
//        this.lagu = new ArrayList<>();
//        this.maxLagu = maxLagu;
//    }
//
//    public void tambahLagu(Lagu lagu) throws LaguSudahAdaException, DaftarPutarPenuhException, LaguTidakValidException {
//        if (lagu == null) {
//            throw new LaguTidakValidException("Lagu tidak boleh null");
//        } else if (judulKosong(lagu.getJudul())) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh kosong");
//        } else if (laguAda(lagu)) {
//            throw new LaguSudahAdaException("Lagu " + lagu.getJudul() + " sudah ada di daftar putar");
//        } else if (lagu.getDurasi() <= 0) {
//            throw new LaguTidakValidException("Durasi lagu tidak boleh <= 0");
//        } else if (lagu.getArtis() == null || lagu.getArtis().isEmpty()) {
//            throw new LaguTidakValidException("Artis lagu tidak boleh kosong");
//        } else if (lagu.getJudul().length() > 100) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh lebih dari 100 karakter");
//        } else if (lagu.getArtis().length() > 50) {
//            throw new LaguTidakValidException("Nama artis tidak boleh lebih dari 50 karakter");
//        } else if (lagu.getDurasi() > 1000) {
//            throw new LaguTidakValidException("Durasi lagu tidak boleh lebih dari 1000 detik");
//        } else if (lagu.getDurasi() < 30) {
//            throw new LaguTidakValidException("Durasi lagu tidak boleh kurang dari 30 detik");
//        } else if (lagu.getArtis().contains(" ")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung spasi");
//        } else if (lagu.getJudul().contains(" ")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung spasi");
//        } else if (lagu.getArtis().contains(",")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung koma");
//        } else if (lagu.getJudul().contains(",")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung koma");
//        }else if (lagu.getArtis().contains(".")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung titik");
//        } else if (lagu.getJudul().contains(".")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung titik");
//        } else if (lagu.getArtis().contains("!")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda seru");
//        } else if (lagu.getJudul().contains("!")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda seru");
//        } else if (lagu.getArtis().contains("?")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda tanya");
//        } else if (lagu.getJudul().contains("?")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda tanya");
//        } else if (lagu.getArtis().contains("/")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung garis miring");
//        } else if (lagu.getJudul().contains("/")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung garis miring");
//        } else if (lagu.getArtis().contains("\\")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung garis miring terbalik");
//        } else if (lagu.getJudul().contains("\\")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung garis miring terbalik");
//        } else if (lagu.getArtis().contains("*")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda bintang");
//        } else if (lagu.getJudul().contains("*")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda bintang");
//        } else if (lagu.getArtis().contains("(")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung kurung buka");
//        } else if (lagu.getJudul().contains("(")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung kurung buka");
//        } else if (lagu.getArtis().contains(")")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung kurung tutup");
//        } else if (lagu.getJudul().contains(")")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung kurung tutup");
//        } else if (lagu.getArtis().contains("-")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda hubung");
//        } else if (lagu.getJudul().contains("-")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda hubung");
//        } else if (lagu.getArtis().contains("_")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung garis bawah");
//        } else if (lagu.getJudul().contains("_")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung garis bawah");
//        } else if (lagu.getArtis().contains("=")) {
//            throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda sama dengan");
//        } else if (lagu.getJudul().contains("=")) {
//            throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda sama dengan");
//        } else if (lagu.getArtis().contains("+")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda tambah");
//            } else if (lagu.getJudul().contains("+")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda tambah");
//            } else if (lagu.getArtis().contains("`")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung backtick");
//            } else if (lagu.getJudul().contains("`")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung backtick");
//            } else if (lagu.getArtis().contains("^")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda pangkat");
//            } else if (lagu.getJudul().contains("^")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda pangkat");
//            } else if (lagu.getArtis().contains("~")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tilde");
//            } else if (lagu.getJudul().contains("~")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tilde");
//            } else if (lagu.getArtis().contains("@")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda at");
//            } else if (lagu.getJudul().contains("@")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda at");
//            } else if (lagu.getArtis().contains("#")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda pagar");
//            } else if (lagu.getArtis().contains("%")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda persen");
//            } else if (lagu.getJudul().contains("%")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda persen");
//            } else if (lagu.getArtis().contains("^")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda caret");
//            } else if (lagu.getJudul().contains("^")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda caret");
//            } else if (lagu.getArtis().contains("&")) {
//                throw new LaguTidakValidException("Nama artis tidak boleh mengandung tanda ampersand");
//            } else if (lagu.getJudul().contains("&")) {
//                throw new LaguTidakValidException("Judul lagu tidak boleh mengandung tanda ampersand");
//            } else if (lagu.size() >= maxLagu) {
//                throw new DaftarPutarPenuhException("Daftar putar sudah penuh");
//            } else {
//                lagu.add(lagu);
//            }
//        }
//
//        private boolean judulKosong(String judul) {
//            return judul == null || judul.isEmpty();
//        }
//
//        private boolean laguAda(Lagu lagu) {
//            for (Lagu laguDiDaftarPutar : lagu) {
//                if (laguDiDaftarPutar.getJudul().equals(lagu.getJudul())) {
//                    return true;
//                }
//            }
//            return false;
//        }
//
//    }
//
//    public class Song {
//
//        public static void main(String[] args) {
//            DaftarPutar daftarPutar = new DaftarPutar("Favorit", "Daftar lagu favorit saya", 10);
//            try {
//                daftarPutar.tambahLagu(new Lagu("Lagu 1", "Penyanyi 1", 120));
//                daftarPutar.tambahLagu(new Lagu("Lagu 2", "Penyanyi 2", 180));
//                daftarPutar.tambahLagu(new Lagu("Lagu 3", "Penyanyi 3", 240));
//                daftarPutar.tambahLagu(new Lagu("Lagu 4", "Penyanyi 4", 300));
//                daftarPutar.tambahLagu(new Lagu("Lagu 5", "Penyanyi 5", 360));
//                daftarPutar.tambahLagu(new Lagu("Lagu 6", "Penyanyi 6", 420));
//                daftarPutar.tambahLagu(new Lagu("Lagu 7", "Penyanyi 7", 480));
//                daftarPutar.tambahLagu(new Lagu("Lagu 8", "Penyanyi 8", 540));
//                daftarPutar.tambahLagu(new Lagu("Lagu 9", "Penyanyi 9", 600));
//                daftarPutar.tambahLagu(new Lagu("Lagu 10", "Penyanyi 10", 660));
//                // Menambahkan lagu ke-11 akan menghasilkan `DaftarPutarPenuhException`
//                daftarPutar.tambahLagu(new Lagu("Lagu 11", "Penyanyi 11", 720));
//            } catch (LaguTidakValidException e) {
//                System.out.println(e.getMessage());
//            } catch (DaftarPutarPenuhException e) {
//                System.out.println(e.getMessage());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//    public class LaguTidakValidException extends Exception {
//
//        public LaguTidakValidException(String message) {
//            super(message);
//        }
//
//    }
//
//    public class DaftarPutarPenuhException extends Exception {
//
//        public DaftarPutarPenuhException(String message) {
//            super(message);
//        }
//
//    }
//
//
//

public class Song{
    public static void main(String[] args) {

    }
}
