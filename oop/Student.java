package oop;

public class Student {
    private String name;
    private Integer id;

    public Student(String name, String id) {
        setName(name);
        setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be null or empty.");
        }
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(String id) {
        this.id = Integer.parseInt(id);
    }

    public static void main(String[] args) {
        try {
            Student student = new Student("Alice", "123");
            System.out.println("Student Name: " + student.getName());
            System.out.println("Student ID: " + student.getId());
            student.setName("Aw");
            student.setId("qq");
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
