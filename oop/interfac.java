package oop;

import java.lang.*;
import java.lang.Override;
public class interfac {
    public static void main(String[] args) {
        PenggunaBiasa pb = new PenggunaBiasa();
    }
}

interface Pengguna {

    void login();

    void logout();

    void postingKonten(String konten);

}

class PenggunaBiasa implements Pengguna {

    @Override
    public void login() {
        System.out.println("Pengguna biasa login");
    }

    @Override
    public void logout() {
        System.out.println("Pengguna biasa logout");
    }

    @Override
    public void postingKonten(String konten) {
        System.out.println("Pengguna biasa posting konten: " + konten);
    }

}

class PenggunaAdmin implements Pengguna {

    @Override
    public void login() {
        System.out.println("Pengguna admin login");
    }

    @Override
    public void logout() {
        System.out.println("Pengguna admin logout");
    }

    @Override
    public void postingKonten(String konten) {
        System.out.println("Pengguna admin posting konten: " + konten);
    }

    public void hapusKonten(String konten) {
        System.out.println("Pengguna admin hapus konten: " + konten);
    }

}
