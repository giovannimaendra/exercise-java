package oop;

enum KategoriProduk {
    ELEKTRONIK,
    PAKAIAN,
    BUKU,
    LAINNYA
}

enum LevelPengguna {
    BIASA,
    SILVER,
    GOLD
}

class Diskon {

    public static double hitungDiskon(KategoriProduk kategori, LevelPengguna level, double harga) {
        double diskon = 0;
        switch (kategori) {
            case ELEKTRONIK:
                diskon = 0.1;
                break;
            case PAKAIAN:
                if (level == LevelPengguna.SILVER) {
                    diskon = 0.2;
                } else if (level == LevelPengguna.GOLD) {
                    diskon = 0.3;
                }
                break;
            case BUKU:
                if (level == LevelPengguna.SILVER) {
                    diskon = 0.15;
                } else if (level == LevelPengguna.GOLD) {
                    diskon = 0.25;
                }
                break;
        }
        return harga * diskon;
    }

}

public class ExampleEnum {

    public static void main(String[] args) {
        double hargaElektronik = 100000;
        double hargaPakaian = 200000;
        double hargaBuku = 50000;

        System.out.println("Diskon Elektronik (Biasa): " + Diskon.hitungDiskon(KategoriProduk.ELEKTRONIK, LevelPengguna.BIASA, hargaElektronik)); // 10000
        System.out.println("Diskon Pakaian (Silver): " + Diskon.hitungDiskon(KategoriProduk.PAKAIAN, LevelPengguna.SILVER, hargaPakaian)); // 40000
        System.out.println("Diskon Buku (Gold): " + Diskon.hitungDiskon(KategoriProduk.BUKU, LevelPengguna.GOLD, hargaBuku)); // 12500
    }

}

