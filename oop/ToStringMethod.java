package oop;

import java.lang.Override;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.*;
class File {

    private String nama;
    private long ukuran;
    private String tipe;
    private Date tanggalDiubah;

    public File(String nama, long ukuran, String tipe, Date tanggalDiubah) {
        this.nama = nama;
        this.ukuran = ukuran;
        this.tipe = tipe;
        this.tanggalDiubah = tanggalDiubah;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getUkuran() {
        return ukuran;
    }

    public void setUkuran(long ukuran) {
        this.ukuran = ukuran;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public Date getTanggalDiubah() {
        return tanggalDiubah;
    }

    public void setTanggalDiubah(Date tanggalDiubah) {
        this.tanggalDiubah = tanggalDiubah;
    }


    @Override
    public String toString() {
        switch (tipe) {
            case "Dokumen":
                return String.format("Dokumen: %s (%s) - Diubah: %s", nama, formatUkuran(ukuran), formatTanggal(tanggalDiubah));
            case "Gambar":
                return String.format("Gambar: %s (%s) - Dimensi: %s x %s - Diubah: %s", nama, formatUkuran(ukuran), getDimensi(), formatTanggal(tanggalDiubah));
            case "Video":
                return String.format("Video: %s (%s) - Durasi: %s - Diubah: %s", nama, formatUkuran(ukuran), formatDurasi(), formatTanggal(tanggalDiubah));
            default:
                return String.format("File: %s (%s) - Diubah: %s", nama, formatUkuran(ukuran), formatTanggal(tanggalDiubah));
        }
    }

    private String formatUkuran(long ukuran) {
        if (ukuran < 1024) {
            return String.format("%d B", ukuran);
        } else if (ukuran < 1048576) {
            return String.format("%.2f KB", ukuran / 1024.0);
        } else if (ukuran < 1073741824) {
            return String.format("%.2f MB", ukuran / 1048576.0);
        } else {
            return String.format("%.2f GB", ukuran / 1073741824.0);
        }
    }

    private String formatTanggal(Date tanggal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        return sdf.format(tanggal);
    }

    private String formatDurasi() {
        long detik = (System.currentTimeMillis() - tanggalDiubah.getTime()) / 1000;
        long jam = detik / 3600;
        long menit = (detik % 3600) / 60;
        detik %= 60;
        return String.format("%02d:%02d:%02d", jam, menit, detik);
    }

    private String getDimensi() {
        // Implementasi untuk mendapatkan dimensi gambar
        return "N/A";
    }

}

public class ToStringMethod {
    public static void main(String[] args) {
        File fileDokumen = new File("laporan.docx", 10240, "Dokumen", new Date());
        File fileGambar = new File("foto.jpg", 2048000, "Gambar", new Date());
        File fileVideo = new File("video.mp4", 1073741824, "Video", new Date());

        System.out.println(fileDokumen); // Dokumen: laporan.docx (10 KB) - Diubah: 2023-11-14 10:29
        System.out.println(fileGambar); // Gambar: foto.jpg (2 MB) - Dimensi: N/A - Diubah: 2023-11-14 10:29
        System.out.println(fileVideo); // Video: video.mp4 (1 GB) - Durasi: 00:00:00 - Diubah: 2023-11-14 10:29
    }
}
