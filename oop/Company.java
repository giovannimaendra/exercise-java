package oop;

class Employee {
    private String name;
    private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }
}

class Manager extends Employee {
    private double bonus;

    public Manager(String name, double salary, double bonus) {
        super(name, salary);
        this.bonus = bonus;
    }

    public double getSalary() {
        return super.getSalary() + bonus;
    }
}

public class Company{
    public static void main(String[] args) {
        Manager manager = new Manager("Raka", 123456, 1000);
        System.out.println(manager.getSalary());

        Employee employee = new Employee("uwo", 60000);
        System.out.println(employee.getSalary());
    }
}