//package oop;
//
//import java.lang.*;
//
//abstract class Buku {
//
//    private String judul;
//    private String penulis;
//    private int tahunTerbit;
//
//    public Buku(String judul, String penulis, int tahunTerbit) {
//        this.judul = judul;
//        this.penulis = penulis;
//        this.tahunTerbit = tahunTerbit;
//    }
//
//    public String getJudul() {
//        return judul;
//    }
//
//    public void setJudul(String judul) {
//        this.judul = judul;
//    }
//
//    public String getPenulis() {
//        return penulis;
//    }
//
//    public void setPenulis(String penulis) {
//        this.penulis = penulis;
//    }
//
//    public int getTahunTerbit() {
//        return tahunTerbit;
//    }
//
//    public void setTahunTerbit(int tahunTerbit) {
//        this.tahunTerbit = tahunTerbit;
//    }
//
//    public abstract double hitungDenda(int hariTerlambat);
//
//}
//
//class BukuFiksi extends Buku {
//
//    public BukuFiksi(String judul, String penulis, int tahunTerbit) {
//        super(judul, penulis, tahunTerbit);
//    }
//
//    @Override
//    public double hitungDenda(int hariTerlambat) {
//        return hariTerlambat * 500;
//    }
//
//}
//
//static class BukuNonFiksi extends Buku {
//
//    public BukuNonFiksi(String judul, String penulis, int tahunTerbit) {
//        super(judul, penulis, tahunTerbit);
//    }
//
//    @Override
//    public double hitungDenda(int hariTerlambat) {
//        return hariTerlambat * 1000;
//    }
//
//}
//
public class Abstraksi{
    public static void main(String[] args) {
//        BukuNonFiksi bnf = new BukuNonFiksi("wawlkaw", "uiuiuiu", 2023);
    }
}