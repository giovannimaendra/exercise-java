package oop;

class Vehicle {
    int speed;

    public void speedUp() {
        speed += 5;
    }
}

class Car extends Vehicle {
    public void speedUp() {
        super.speedUp();
        speed += 10;
    }
}
public class Override {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.speedUp();
        System.out.println(vehicle.speed);

        Car car = new Car();
        car.speedUp();
        System.out.println(car.speed);
    }
}
