package oop;

import java.util.Scanner;

public class RuntimeExceptionDemo {

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Masukkan bilangan bulat: ");
            int bilangan = Integer.parseInt(scanner.nextLine());
            int hasil = bilangan * 10;
            System.out.println("Hasil perkalian: " + hasil);
        } catch (NumberFormatException e) {
            System.out.println("Input tidak valid: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("Hasil perkalian melebihi batas maksimum: " + e.getMessage());
        }
    }

}
