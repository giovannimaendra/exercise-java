package example;

public class Variable {
    public static void main(String[] args){
        int num = 10;
        System.out.println("Bilangan bulat: " + num);
        double dec = 3.14;
        System.out.println("Bilangan desimal: " + dec);
        char character = 'A';
        System.out.println("Karakter: " + character);
        String string = "Hello World!";
        System.out.println("String: " + string);

        int number = 10;
        double decimal1 = number; // Widening casting, tidak perlu explicit cast
        System.out.println("Bilangan desimal: " + decimal1);
        double decimal = 3.14;
        int number2 = (int) decimal; // Narrowing casting, perlu explicit cast
        System.out.println("Bilangan bulat: " + number2);

    }
}
