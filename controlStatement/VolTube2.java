package controlStatement;

import java.util.Scanner;

public class VolTube2 {
    public static void main(String[] args) {
        final double pi = 3.14;

        Scanner sc = new Scanner(System.in);
        System.out.print("Radius : ");
        int radius = sc.nextInt();
        if(radius <= 0){
            System.out.println("Tidak bisa memasukan nilai dibawah 1");
            return ;
        }
        System.out.print("Height : ");
        int height = sc.nextInt();
        if(height <= 0){
            System.out.println("Tidak bisa memasukan nilai dibawah 1");
            return ;
        }
        sc.close();

        double area = pi*radius*radius*height;
        System.out.println(area);
    }
}
