package controlStatement;

import java.util.Scanner;

public class TahunKabisat2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan tahun: ");
        int tahun = input.nextInt();

        String hasil = ((tahun % 4 == 0 && tahun % 100 != 0) || tahun % 400 == 0) ?
                tahun + " adalah tahun kabisat" :
                tahun + " bukan tahun kabisat";
        System.out.println(hasil);
    }
}
