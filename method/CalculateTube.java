package method;

import java.util.Scanner;

public class CalculateTube {
    // Konstanta untuk nilai Pi
    final static double PI = 3.14;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan jari-jari tabung: ");
        double radius = scanner.nextDouble();

        System.out.print("Masukkan tinggi tabung: ");
        double height = scanner.nextDouble();

        double luas = hitungLuas(radius, height);
        double volume = hitungVolume(radius, height);

        System.out.println("Luas permukaan tabung adalah: " + luas);
        System.out.println("Volume tabung adalah: " + volume);
    }

    // Metode untuk menghitung luas permukaan tabung
    public static double hitungLuas(double radius, double height) {
        return 2 * PI * radius * (radius + height);
    }

    // Metode untuk menghitung volume tabung
    public static double hitungVolume(double radius, double height) {
        return PI * radius * radius * height;
    }
}
