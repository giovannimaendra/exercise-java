package method;

import java.util.Scanner;

public class JumlahRekursif {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan angka: ");
        int angka = input.nextInt();

        int hasil = jumlahkan(angka);
        System.out.println("Hasil penjumlahan dari 1 hingga " + angka + " adalah: " + hasil);
    }

    public static int jumlahkan(int n) {
        if (n <= 1) {
            return n;
        } else {
            return n + jumlahkan(n - 1);
        }
    }
}
