package method;

import java.util.Scanner;

public class JumlahDigitRekursif {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan angka yang ingin dijumlahkan digit-digitnya: ");
        int angka = input.nextInt();

        int hasil = jumlahkanDigit(angka);
        System.out.println("Hasil penjumlahan digit-digit angka " + angka + " adalah: " + hasil);
    }

    public static int jumlahkanDigit(int n) {
        if (n == 0) {
            return 0;
        } else {
            return n % 10 + jumlahkanDigit(n / 10);
        }
    }
}
